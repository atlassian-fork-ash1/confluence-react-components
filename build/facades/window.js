"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.addEventListener = addEventListener;
exports.removeEventListener = removeEventListener;
function addEventListener() {
    window.addEventListener.apply(window, arguments);
}

function removeEventListener() {
    window.removeEventListener.apply(window, arguments);
}