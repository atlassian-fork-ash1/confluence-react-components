'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _document = require('../facades/document');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ContentBodyRest = function (_Component) {
    _inherits(ContentBodyRest, _Component);

    function ContentBodyRest() {
        _classCallCheck(this, ContentBodyRest);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(ContentBodyRest).call(this));

        _this._setScriptContainerRef = _this._setScriptContainerRef.bind(_this);
        _this._setBodyContainerRef = _this._setBodyContainerRef.bind(_this);
        return _this;
    }

    _createClass(ContentBodyRest, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this._updateAnchorTargets();
            this._loadJSDependencies();
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate() {
            this._updateAnchorTargets();
            this._loadJSDependencies();
        }
    }, {
        key: '_updateAnchorTargets',
        value: function _updateAnchorTargets() {
            var anchors = this._bodyContainer.querySelectorAll('a');
            for (var i = 0, anchor; anchor = anchors[i]; i++) {
                anchor.target = '_top';
            }
        }
    }, {
        key: '_loadJSDependencies',
        value: function _loadJSDependencies() {
            var content = this.props.content;
            content.jsDependencies.forEach(this._appendScriptToContainer, this);
        }
    }, {
        key: '_appendScriptToContainer',
        value: function _appendScriptToContainer(uri) {
            var scriptElement = (0, _document.createElement)('script');
            scriptElement.async = true;
            scriptElement.src = uri;
            this._scriptContainer.appendChild(scriptElement);
        }
    }, {
        key: '_setBodyContainerRef',
        value: function _setBodyContainerRef(node) {
            this._bodyContainer = node;
        }
    }, {
        key: '_setScriptContainerRef',
        value: function _setScriptContainerRef(node) {
            this._scriptContainer = node;
        }
    }, {
        key: 'render',
        value: function render() {
            var _props$content = this.props.content;
            var body = _props$content.body;
            var cssDependencies = _props$content.cssDependencies;


            return _react2.default.createElement(
                'div',
                { id: 'content', className: 'page view' },
                _react2.default.createElement('div', { dangerouslySetInnerHTML: { __html: '' + body + cssDependencies }, ref: this._setBodyContainerRef, id: 'main-content', className: 'wiki-content' }),
                _react2.default.createElement('div', { ref: this._setScriptContainerRef })
            );
        }
    }]);

    return ContentBodyRest;
}(_react.Component);

exports.default = ContentBodyRest;


ContentBodyRest.displayName = 'ContentBodyRest';
ContentBodyRest.propTypes = {
    /**
     * The ID of the content to render.
     */
    contentId: _react.PropTypes.string
};