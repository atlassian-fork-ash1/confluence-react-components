'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _sinon = require('sinon');

var _sinon2 = _interopRequireDefault(_sinon);

var _sinonChai = require('sinon-chai');

var _sinonChai2 = _interopRequireDefault(_sinonChai);

var _enzyme = require('enzyme');

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _chaiEnzyme = require('chai-enzyme');

var _chaiEnzyme2 = _interopRequireDefault(_chaiEnzyme);

var _proxyquire = require('proxyquire');

var _proxyquire2 = _interopRequireDefault(_proxyquire);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

_chai2.default.use((0, _chaiEnzyme2.default)());
_chai2.default.use(_sinonChai2.default);

var WINDOW = '../facades/window';
var UUID = '../util/uuid';

describe('ContentBodyIframe', function () {
    var mocks = void 0;
    var ContentBodyIframe = void 0;

    beforeEach(function () {
        var _mocks;

        mocks = (_mocks = {}, _defineProperty(_mocks, UUID, {
            default: _sinon2.default.stub()
        }), _defineProperty(_mocks, WINDOW, {
            addEventListener: _sinon2.default.stub(),
            removeEventListener: _sinon2.default.stub()
        }), _mocks);

        ContentBodyIframe = (0, _proxyquire2.default)('../content-body-iframe', mocks).default;
    });

    describe('render', function () {
        var wrapper = void 0;
        var uuid = 'foobar!';
        var id = '123';
        var props = {
            baseUrl: 'http://www.atlassian.com',
            content: { id: id },
            onContentLoaded: _sinon2.default.spy()
        };

        beforeEach(function () {
            mocks[UUID].default.returns(uuid);
            wrapper = (0, _enzyme.shallow)(_react2.default.createElement(ContentBodyIframe, props));
        });

        it('Should render a iframe', function () {
            (0, _chai.expect)(wrapper).to.have.descendants('iframe');

            var iframe = wrapper.find('iframe');
            (0, _chai.expect)(iframe).to.have.prop('src').equal(props.baseUrl + '/confluence/content-only/viewpage.action?pageId=' + id + '&iframeId=' + uuid);
            (0, _chai.expect)(iframe).to.have.prop('onLoad').equal(props.onContentLoaded);
        });

        it('if `contextPath` prop not passed it is "/confluence"', function () {
            (0, _chai.expect)(wrapper.find('iframe')).to.have.prop('src').equal(props.baseUrl + '/confluence/content-only/viewpage.action?pageId=' + id + '&iframeId=' + uuid);
        });

        it('use passed `contextPath` for iframe', function () {
            wrapper.setProps({ contextPath: '/wiki' });
            (0, _chai.expect)(wrapper.find('iframe')).to.have.prop('src').equal(props.baseUrl + '/wiki/content-only/viewpage.action?pageId=' + id + '&iframeId=' + uuid);

            wrapper.setProps({ contextPath: '' });
            (0, _chai.expect)(wrapper.find('iframe')).to.have.prop('src').equal(props.baseUrl + '/content-only/viewpage.action?pageId=' + id + '&iframeId=' + uuid);
        });
    });

    describe('componentDidMount', function () {
        var wrapper = void 0;
        var instance = void 0;
        var uuid = void 0;

        beforeEach(function () {
            uuid = 'barfoo';
            mocks[UUID].default.returns(uuid);
            wrapper = (0, _enzyme.shallow)(_react2.default.createElement(ContentBodyIframe, { baseUrl: '', content: {} }));
            instance = wrapper.instance();
            instance.componentDidMount();
        });

        it('Should setup a message event listener', function () {
            (0, _chai.expect)(mocks[WINDOW].addEventListener).to.have.been.calledWith('message', instance.receiveMessage);
        });

        describe('receiveMessage handler', function () {
            var receiveMessage = void 0;
            beforeEach(function () {
                receiveMessage = mocks[WINDOW].addEventListener.getCall(0).args[1];
            });

            describe('When iframeId doesn\'t match the iframeId of the component', function () {
                it('Should return undefined', function () {
                    (0, _chai.expect)(receiveMessage({
                        data: {
                            iframeId: 'something',
                            height: 1234
                        }
                    })).to.equal();
                });
            });

            describe('When event iframeId matches the iframeId of the component', function () {
                it('Should setState with the height in the message', function () {
                    var height = 599;
                    receiveMessage({
                        data: {
                            iframeId: uuid,
                            height: height
                        }
                    });

                    (0, _chai.expect)(wrapper).to.have.state('height', height + 'px');
                });
            });
        });

        describe('componentWillUnmount', function () {
            beforeEach(function () {
                instance.componentWillUnmount();
            });

            it('Should remove the message event listener', function () {
                (0, _chai.expect)(mocks[WINDOW].removeEventListener).to.have.been.calledWith('message', instance.receiveMessage);
            });
        });
    });
});