import React from 'react';

import sinon, { spy, stub } from 'sinon';
import sinonChai from 'sinon-chai';
import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import proxyquire from 'proxyquire';

chai.use(chaiEnzyme());
chai.use(sinonChai);

const WINDOW = '../facades/window';
const UUID = '../util/uuid';

describe('ContentBodyIframe', () => {
    let mocks;
    let ContentBodyIframe;

    beforeEach(() => {
        mocks = {
            [UUID]: {
                default: sinon.stub()
            },
            [WINDOW]: {
                addEventListener: sinon.stub(),
                removeEventListener: sinon.stub()
            }
        };

        ContentBodyIframe = proxyquire('../content-body-iframe', mocks).default;
    });

    describe('render', () => {
        let wrapper;
        const uuid = 'foobar!';
        const id = '123';
        const props = {
            baseUrl: 'http://www.atlassian.com',
            content: {id},
            onContentLoaded: sinon.spy()
        };

        beforeEach(() => {
            mocks[UUID].default.returns(uuid);
            wrapper = shallow(<ContentBodyIframe {...props} />);
        });

        it('Should render a iframe', () => {
            expect(wrapper).to.have.descendants('iframe');

            const iframe = wrapper.find('iframe');
            expect(iframe).to.have.prop('src').equal(`${props.baseUrl}/confluence/content-only/viewpage.action?pageId=${id}&iframeId=${uuid}`);
            expect(iframe).to.have.prop('onLoad').equal(props.onContentLoaded);
        });

        it('if `contextPath` prop not passed it is "/confluence"', () => {
            expect(wrapper.find('iframe')).to.have.prop('src').equal(`${props.baseUrl}/confluence/content-only/viewpage.action?pageId=${id}&iframeId=${uuid}`);
        });

        it('use passed `contextPath` for iframe', () => {
            wrapper.setProps({contextPath: '/wiki'});
            expect(wrapper.find('iframe')).to.have.prop('src').equal(`${props.baseUrl}/wiki/content-only/viewpage.action?pageId=${id}&iframeId=${uuid}`);

            wrapper.setProps({contextPath: ''});
            expect(wrapper.find('iframe')).to.have.prop('src').equal(`${props.baseUrl}/content-only/viewpage.action?pageId=${id}&iframeId=${uuid}`);
        });
    });

    describe('componentDidMount', () => {
        let wrapper;
        let instance;
        let uuid;

        beforeEach(() => {
            uuid = 'barfoo';
            mocks[UUID].default.returns(uuid);
            wrapper = shallow(<ContentBodyIframe baseUrl="" content={{}}/>);
            instance = wrapper.instance();
            instance.componentDidMount();
        });

        it('Should setup a message event listener', () => {
            expect(mocks[WINDOW].addEventListener).to.have.been.calledWith('message', instance.receiveMessage);
        });

        describe('receiveMessage handler', () => {
            let receiveMessage;
            beforeEach(() => {
                receiveMessage = mocks[WINDOW].addEventListener.getCall(0).args[1];
            });

            describe(`When iframeId doesn't match the iframeId of the component`, () => {
                it('Should return undefined', () => {
                    expect(receiveMessage({
                        data: {
                            iframeId: 'something',
                            height: 1234
                        }
                    })).to.equal();
                });
            });

            describe('When event iframeId matches the iframeId of the component', () => {
                it('Should setState with the height in the message', () => {
                    const height = 599;
                    receiveMessage({
                        data: {
                            iframeId: uuid,
                            height
                        }
                    });

                    expect(wrapper).to.have.state('height', `${height}px`);
                });
            });
        });

        describe('componentWillUnmount', () => {
            beforeEach(() => {
                instance.componentWillUnmount();
            });

            it('Should remove the message event listener', () => {
                expect(mocks[WINDOW].removeEventListener).to.have.been.calledWith('message', instance.receiveMessage);
            });
        });
    });
});
