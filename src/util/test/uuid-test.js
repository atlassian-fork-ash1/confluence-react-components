import chai, { expect } from 'chai';
import uuid from '../uuid';

describe('uuid', () => {
    const uuidRegex = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/;

    it('Should return a uuid', function() {
        expect(uuid()).to.match(uuidRegex);
    });

    it('Should be fairly random', () => {
        const uuids = [];
        for(let i = 0; i < 1000; i++) {
            const newUuid = uuid();
            expect(uuids.indexOf(newUuid)).to.equal(-1);
            uuids.push(newUuid);
        }

    });
});
