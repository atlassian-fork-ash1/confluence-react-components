import React from 'react';

import sinon, { spy, stub } from 'sinon';
import sinonChai from 'sinon-chai';
import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import proxyquire from 'proxyquire';

chai.use(chaiEnzyme());
chai.use(sinonChai);

const DOCUMENT = '../facades/document';

describe('ContentBodyRest', () => {
    let mocks;
    let ContentBodyRest;

    beforeEach(() => {
        mocks = {
            [DOCUMENT]: {
                createElement: tag => {
                    return {tag};
                }
            }
        };

        ContentBodyRest = proxyquire('../content-body-rest', mocks).default;
    });

    describe('Render', () => {
        let content;

        class MockDOMNode {
            constructor() {
                this.querySelectorAll = sinon.stub().returns([]);
                this.appendChild = sinon.stub();
            }
        }

        beforeEach(() => {
            content = {
                body: '<html></html>',
                cssDependencies: '<link></link>'
            };
        });

        it('Should contain 3 divs', () => {
            const wrapper = shallow(<ContentBodyRest content={content}/>);
            expect(wrapper).to.have.exactly(3).descendants('div');
        });

        it('Should have the correct content container classes', () => {
            const wrapper = shallow(<ContentBodyRest content={content}/>);
            const firstContainer = wrapper.find('#content');
            expect(firstContainer).to.exist;
            const secondContainer = wrapper.find('#main-content');
            expect(secondContainer).to.exist;
            expect(firstContainer).to.have.className('page view');
            expect(secondContainer).to.have.className('wiki-content');
        });

        it('Should contain the given body and cssDependencies', () => {
            const wrapper = shallow(<ContentBodyRest content={content}/>);
            const htmlDiv = wrapper.find('[dangerouslySetInnerHTML]');
            expect(htmlDiv).to.have.prop('dangerouslySetInnerHTML').deep.equal({__html: `${content.body}${content.cssDependencies}`});
        });

        it('Should set correct references', () => {
            const wrapper = shallow(<ContentBodyRest content={content}/>);
            const instance = wrapper.instance();
            const children = wrapper.find('#content').children();
            expect(children.at(0).node.ref).to.equal( instance._setBodyContainerRef);
            children.at(0).node.ref('body-node');
            expect(instance._bodyContainer).to.equal('body-node');

            expect(children.at(1).node.ref).to.equal( instance._setScriptContainerRef);
            children.at(1).node.ref('scripts-node');
            expect(instance._scriptContainer).to.equal('scripts-node');
        });

        describe('lifeCycleEvents', () => {
            let wrapper;
            let instance;

            beforeEach(() => {
                content.jsDependencies = [
                    'www.google.com',
                    'www.someotherdomain.com'
                ];
                wrapper = shallow(<ContentBodyRest content={content}/>);

                instance = wrapper.instance();
                instance._bodyContainer = new MockDOMNode();
                instance._scriptContainer = new MockDOMNode();
            });

            describe('componentDidUpdate', () => {
                it('Should add target: "_top" to all anchors in the component', () => {
                    const nodeList = [
                        {},
                        {},
                        {},
                        {}
                    ];

                    instance._bodyContainer.querySelectorAll.returns(nodeList);

                    instance.componentDidUpdate();

                    nodeList.forEach(node => expect(node.target).to.equal('_top'));
                });
                it('Should add script elements for each jsDependency', () => {
                    instance.componentDidUpdate();
                    content.jsDependencies.forEach(src => expect(instance._scriptContainer.appendChild).to.have.been.calledWith({
                        async: true,
                        src,
                        tag: 'script'
                    }));
                });
            });

            describe('componentDidMount', () => {
                it('Should append script elements with the jsDependencies', () => {
                    instance.componentDidMount();
                    content.jsDependencies.forEach(src => expect(instance._scriptContainer.appendChild).to.have.been.calledWith({
                        async: true,
                        src,
                        tag: 'script'
                    }));
                });

                it('Should add target: "_top" to all anchors in the component', () => {
                    const nodeList = [
                        {},
                        {},
                        {}
                    ];

                    instance._bodyContainer.querySelectorAll.returns(nodeList);

                    instance.componentDidMount();

                    nodeList.forEach(node => expect(node.target).to.equal('_top'));
                });
            });
        });
    });
});
