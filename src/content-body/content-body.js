import React, { Component, PropTypes } from 'react';
import ContentBodyIframe from '../content-body-iframe/content-body-iframe';
import ContentBodyRest from '../content-body-rest/content-body-rest';

export default class ContentBody extends Component {
    componentDidMount() {
        this._finishLoadingIfSPAFriendly(this.props);
    }

    componentDidUpdate() {
        this._finishLoadingIfSPAFriendly(this.props);
    }

    _finishLoadingIfSPAFriendly(props) {
        const { content, onContentLoaded } = props;
        if (content.isSPAFriendly && onContentLoaded) {
            onContentLoaded();
        }
    }

    render() {
        const { content, baseUrl, contextPath, onContentLoaded } = this.props;

        if (!content.isSPAFriendly) {
            return <ContentBodyIframe baseUrl={baseUrl} content={content} contextPath={contextPath} onContentLoaded={onContentLoaded} />;
        }

        return <ContentBodyRest content={content} />;
    }
}

ContentBody.displayName = 'ContentBody';
ContentBody.propTypes = {
    /**
     * The ID of the content to render.
     */
    baseUrl: PropTypes.string,
    content: React.PropTypes.shape({
        id: PropTypes.string,
        body: React.PropTypes.string,
        cssDependencies: React.PropTypes.string,
        jsDependencies: React.PropTypes.array.isRequired,
        isSPAFriendly: React.PropTypes.bool.isRequired
    }),
    /**
     * Callback when content finishes loading.
     */
    onContentLoaded: PropTypes.func
};
